import os
import pytest
import shlex
from configparser import ConfigParser
import io
from pathlib import Path


configvar = 'ASE_ENGINES_CONFIG'


def _configpaths():
    configpaths = os.environ.get(configvar)
    if configpaths:
        return configpaths.split(':')
    else:
        return [Path.home() / '.config/ase/ase.conf']


configpaths = _configpaths()
parser = ConfigParser()
loaded_configpaths = parser.read(configpaths)


@pytest.fixture(scope='session')
def config():
    return parser


@pytest.fixture
def aims(config):
    return config['aims']


def pytest_report_header(config, startdir):
    if configpaths:
        header = '-------- CONFIGURATION ----------'
        yield header
        buf = io.StringIO()
        parser.write(buf)
        yield buf.getvalue()
        yield '-' * len(header)

    paths = ''.join(str(path) for path in configpaths)
    yield '$ASE_ENGINES_CONFIG: {}'.format(
        os.environ.get('ASE_ENGINES_CONFIG'))
    yield 'Loaded config paths: {}'.format(paths)
