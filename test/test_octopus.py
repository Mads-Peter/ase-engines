import os
import pytest
from engines.octopus.octopus import main, Octopus  # groundstate
from ase.calculators.octopus import OctopusProfile


@pytest.fixture(scope='session')
def profile(config):
    print(config)
    # TODO: get binary and pseudopotential paths from configfile
    exes = config['executables']
    if 'octopus' not in exes:
        pytest.skip(reason='no octopus in configuration')
    return OctopusProfile(exes['octopus'])


@pytest.fixture(scope='session')
def octopus(profile):
    return Octopus(profile)


@pytest.fixture(scope='session')
def octopus_gs(tmp_path_factory, octopus):
    directory = tmp_path_factory.mktemp('octopus-gs')
    from ase.build import bulk

    atoms = bulk('Si')
    parameters = dict(
        calculationmode='gs',
        kpointsgrid=[[4, 4, 4]],
        kpointsusesymmetries=True,
        extrastates=1,
        spacing='0.5')

    return octopus.groundstate(atoms, parameters, directory)


def test_groundstate(octopus_gs):
    assert (octopus_gs.directory / 'inp').exists()
    # TODO more actual testing goes here
