import pytest
import numpy as np
from pathlib import Path

def test_dielectric(aln_dielectric, aln_groundstate):
    from ase.utils import workdir
    from abipy.flowtk.tasks import TaskManager
    pytest.skip('This test is not working yet.')
    with workdir(aln_dielectric.directory): # Does not need to be any specific directory. 
        manager_text = TaskManager.get_simple_manager()
        with open('manager.yml', 'w') as fd:
            print(manager_text, file=fd)
        try:
            dielectric_properties = aln_dielectric.read_results()
        except Exception as e:
            files_to_read = ['run.err', 'run.out']
            for file in files_to_read:
                with open(file, 'r') as fd:
                    print(fd.read())
            raise e

        groundstate_properties = aln_groundstate.read_results()

    assert dielectric_properties.get('dielectric_tensor', None) is not None
    assert dielectric_properties.get('born_effective_charges', None) is not None

    dr_diel = dielectric_properties['dielectric_tensor']
    dr_bec = dielectric_properties['born_effective_charges']
    natoms = len(aln_dielectric.atoms)

    assert dr_diel.shape == (3, 3), 'Wrong shape for the dielectric tensor'
    assert dr_bec.shape == (natoms, 3, 3), 'Wrong shape for the born effective charges'

    np.testing.assert_allclose(dr_diel[0, 0], 1.06817417e+01, atol=1e-5)
    np.testing.assert_allclose(dr_bec[0, 0, 0], 3.73206370e+00, atol=1e-5)

    expected_path = aln_dielectric.directory / 'calco_DS2_DDB'
    assert Path.exists(expected_path), f'Expected path {expected_path} does not exist.'
