import numpy as np


def test_relax_properties(si_relax):
    properties = si_relax.read_results()
    assert properties.get('energy', None) is not None
    assert properties.get('forces', None) is not None
    assert properties.get('stress', None) is not None

def test_relax_energy(si_relax):
    properties = si_relax.read_results()
    np.testing.assert_allclose(properties['energy'], -214.03084784, atol=1e-5)
